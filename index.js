/*Load the expressjs module into our application and saved it in a variable called express*/
const express = require('express');

/*
	This creates an application that uses express and store it as app
	app is our server
*/
const app = express();

const port = 4000;

// middleware
app.use(express.json())

// mock data
let users = [
	{
		username: "BMadrigal",
		email: "fateReader@gmail.com",
		password: "dontTalkAboutMe"
	},
	{
		username: "Luisa",
		email: "stronggirl@gmail.com",
		password: "pressure"
	}
];

let items = [
	{
		name: "roses",
		price: 170,
		isActive: true
	},
	{
		name: "tulips",
		price: 250,
		isActive: true
	}
];


// app.get(<endpoint>, <function for req and res>)

app.get('/', (req, res) => {

	res.send('Hello from my first expressJS API')
	//res.status(200).send('Hello from my first expressJS API')
});

//MINI ACTIVITY
/*app.get('/greeting', (req, res) => {

	res.send('Hello from Batch182-Collina')

});
*/

// retrieval of the mock database
app.get('/users', (req, res) => {

	//res.send already stringifies for you
	res.send(users);
	//res.json(users);
});


app.post('/users', (req, res) => {

	console.log(req.body); //result: {}

	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	}

	//push newUser into users array
	users.push(newUser);
	console.log(users);

	//send the updated users array in the client
	res.send(users);
	});


//Delete method
app.delete('/users', (req, res) => {

	users.pop();
	console.log(users);

	//send the updated users array
	res.send(users);
});


//PUT method (update)
//update user's password - goal
// :index = wildcard
// url: localhost:4000/users/0
app.put('/users/:index', (req, res) => {

	console.log(req.body)

	// an object that contains the value if URL params
	console.log(req.params)

	// parseInt the value of the number coming from req.params
	// ['0'] turns into [0]
	let index = parseInt(req.params.index);

	// users[0].password
	users[index].password = req.body.password;

	res.send(users[index]);
});

//MINI ACTIVITY
app.put('/users/update/:index', (req, res) => {

	console.log(req.body)

	// an object that contains the value if URL params
	console.log(req.params)

	// parseInt the value of the number coming from req.params
	// ['0'] turns into [0]
	let index = parseInt(req.params.index);

	// users[0].username
	users[index].username = req.body.username;

	res.send(users[index]);
});

app.get('/users/getSingleUser/:index',(req, res) => {

	console.log(req.params) //result: {index: '1'}

	let index = parseInt(req.params.index) //parseInt('1')

	console.log(index); //result: 1

	res.send(users[index]);
	console.log(users[index]); //result: undefined

})


//ACTIVITY #1

app.get('/items', (req, res) => {

	res.send(items);
});


app.post('/items/', (req, res) => {

	let newItem = {
		name: req.body.name,
		price: req.body.price,
		isActive: req.body.isActive
	}

	items.push(newItem);

	res.send(items);
});


app.put('/items/:index', (req, res) => {

	let index = parseInt(req.params.index);

	items[index].price = req.body.price;

	res.send(items[index]);
});

//ACTIVITY #2

app.get('/items/getSingleItem/:index', (req, res) => {

	let index = parseInt(req.params.index)

	res.send(items[index]);
});

app.put('/items/archive/:index', (req, res) => {

	let index = parseInt(req.params.index);

	items[index].isActive = false;

	res.send(items[index]);
});


app.put('/items/activate/:index', (req, res) => {

	let index = parseInt(req.params.index);

	items[index].isActive = true;

	res.send(items[index]);
});

// port
app.listen(port, () => console.log(`Server is running at port ${port}`))
